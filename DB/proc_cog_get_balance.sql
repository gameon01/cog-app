USE cog;
SELECT database();
DROP PROCEDURE IF EXISTS proc_cog_get_balance;
DELIMITER //
CREATE PROCEDURE proc_cog_get_balance( 
    IN      in_user_id VARCHAR(50) ,
    OUT     out_balance double ,
	OUT     out_result INT ,
    OUT     out_err_msg VARCHAR(500)
)
proc_cog_get_balance:BEGIN
    set out_result = 1;
	set out_err_msg = '';
    SELECT c_balance INTO out_balance
        FROM t_balance
        WHERE c_username = in_user_id; 
END //
    
DELIMITER ;

USE cog;
SELECT database();
DROP PROCEDURE IF EXISTS proc_cog_add_beneficiary;
DELIMITER //
CREATE PROCEDURE proc_cog_add_beneficiary( 
    IN      in_user_id VARCHAR(50) ,
    IN      in_beneficiary_name VARCHAR(50) ,
	IN      in_account_number VARCHAR(50) ,
    IN      in_ifsc_code VARCHAR(50) ,
	IN      in_nick_name VARCHAR(50) ,	
    OUT     out_result INT ,
    OUT     out_err_msg VARCHAR(500)
)
proc_cog_add_beneficiary:BEGIN
    DECLARE v_count TINYINT;
	DECLARE v_user_count TINYINT;
    DECLARE v_error_code TINYINT;
	DECLARE v_error_msg VARCHAR(200);
    DECLARE v_id VARCHAR(200);
	
    SET v_count = 0;
	SET v_user_count = 0;
    SET out_err_msg = '';
    SELECT 1 INTO v_count
        FROM t_beneficiary
        WHERE c_username = in_user_id and c_ben_acc_number = in_account_number;
	SELECT 1 into v_user_count from t_account where c_accntno = in_account_number;
    IF(v_count > 0) THEN
        SET out_result = -1;
        SET out_err_msg = 'This beneficiary is already added';
	ELSEIF(v_user_count = 0) THEN
		SET out_result = -2;
        SET out_err_msg = 'Beneficiary account not found';
     ELSE
        SET v_count = 0;
        INSERT INTO t_beneficiary(c_username,c_beneficiary_name, c_ben_acc_number, c_beneficiary_ifsc_code, c_nick_name, c_create_time) VALUES (in_user_id, in_beneficiary_name, in_account_number, in_ifsc_code, in_nick_name, CURRENT_TIMESTAMP );
		SELECT LAST_INSERT_ID() INTO v_id;
		SET out_result = 1;
    END IF;        
    
  COMMIT;
END //
    
DELIMITER ;

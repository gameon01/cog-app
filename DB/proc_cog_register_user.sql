USE cog;
SELECT database();
DROP PROCEDURE IF EXISTS proc_cog_register_user;
DELIMITER //
CREATE PROCEDURE proc_cog_register_user( 
    IN      in_user_id VARCHAR(50) ,
    IN      in_password VARCHAR(50) ,
	IN 		in_first_name VARCHAR(40) ,
	IN 		in_last_name VARCHAR(40) ,
	IN 		in_address VARCHAR(200) ,
	IN 		in_account_no VARCHAR(20) ,
    OUT     out_result INT ,
    OUT     out_err_msg VARCHAR(500)
)
proc_cog_register_user:BEGIN
    DECLARE v_count TINYINT;
    DECLARE v_error_code TINYINT;
	DECLARE v_error_msg VARCHAR(200);
	DECLARE v_id VARCHAR(50);
    
    SET v_count = 0;
    SET out_err_msg = '';
    SELECT 1 INTO v_count
        FROM t_account
        WHERE c_username = in_user_id limit 1;
    IF(v_count > 0) THEN
        SET out_result = -1;
        SET out_err_msg = 'User already registered';
    ELSE
        SET v_count = 0;
        INSERT INTO t_account(c_username,c_password, c_firstname, c_lastname, c_address, c_accntno, c_create_time) VALUES (in_user_id, in_password, in_first_name, in_last_name, in_address, in_account_no, CURRENT_TIMESTAMP );
		SELECT LAST_INSERT_ID() INTO v_id;
		SET out_result = 1;
    END IF;        
    
  COMMIT;
END //
    
DELIMITER ;

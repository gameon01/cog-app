USE cog;
SELECT database();
DROP PROCEDURE IF EXISTS proc_cog_deposit;
DELIMITER //
CREATE PROCEDURE proc_cog_deposit( 
    IN      in_user_id VARCHAR(50) ,
    IN      in_amount double ,
	IN      in_txn_type VARCHAR(50) ,
    OUT     out_result INT ,
    OUT     out_err_msg VARCHAR(500)
)
proc_cog_deposit:BEGIN
    DECLARE v_balance double;
    DECLARE v_error_code TINYINT;
	DECLARE v_error_msg VARCHAR(200);
    DECLARE v_id VARCHAR(200);
	DECLARE v_old_balance double DEFAULT 0;
	

    SET out_err_msg = '';
    SELECT c_balance INTO v_old_balance
        FROM t_balance
        WHERE c_username = in_user_id;
	set v_balance = v_old_balance + in_amount;
	IF(v_old_balance > 0) THEN
	UPDATE t_balance set c_balance = v_balance where c_username = in_user_id;
	ELSE
    INSERT INTO t_balance(c_username, c_balance, c_create_time) VALUES (in_user_id, v_balance, CURRENT_TIMESTAMP );
	END IF;
	INSERT INTO t_transaction_history(c_username, c_old_balance, c_new_balance, c_txn_type, c_transfor_to, c_transfor_from, c_txn_date) VALUES(in_user_id, v_old_balance, v_balance, in_txn_type, 'self', 'self', CURRENT_TIMESTAMP);
	SELECT LAST_INSERT_ID() INTO v_id;
	SET out_result = 1;      
    
  COMMIT;
END //
    
DELIMITER ;

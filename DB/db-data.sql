CREATE TABLE t_account(
	c_username VARCHAR(100) NOT NULL,
	c_password VARCHAR(100) NOT NULL,
    c_firstname VARCHAR(40),
    c_lastname VARCHAR(40),
	c_address VARCHAR(200),
    c_accntno VARCHAR(50) UNIQUE,
	c_create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (c_username)
);

CREATE TABLE t_balance(
	c_username VARCHAR(100) NOT NULL,
	c_balance DOUBLE DEFAULT 0,
	c_create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (c_username)
);

CREATE TABLE t_transaction_history(
	c_username VARCHAR(100) NOT NULL REFERENCES t_account(c_username),
	c_old_balance DOUBLE DEFAULT 0,
	c_new_balance DOUBLE DEFAULT 0,
	c_txn_type VARCHAR(20) DEFAULT NULL,
	c_txn_id BIGINT NOT NULL AUTO_INCREMENT UNIQUE,
	c_transfor_to VARCHAR(100),
	c_transfor_from VARCHAR(100),
	c_txn_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (c_username, c_txn_id)
);

CREATE TABLE t_beneficiary(
	c_username VARCHAR(100) NOT NULL REFERENCES t_account(c_username),
	c_beneficiary_name VARCHAR(100) NOT NULL,
	c_ben_acc_number VARCHAR(100) NOT NULL REFERENCES t_account(c_accntno),
	c_beneficiary_ifsc_code VARCHAR(100) NOT NULL,
	c_nick_name VARCHAR(100) NOT NULL,
	c_create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (c_username, c_ben_acc_number)
);
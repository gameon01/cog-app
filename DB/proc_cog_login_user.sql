USE cog;
SELECT database();
DROP PROCEDURE IF EXISTS proc_cog_login_user;
DELIMITER //
CREATE PROCEDURE proc_cog_login_user( 
    IN      in_user_id VARCHAR(50) ,
    IN      in_password VARCHAR(50) ,
    OUT     out_result INT ,
    OUT     out_err_msg VARCHAR(500)
)
proc_cog_login_user:BEGIN
    DECLARE v_count TINYINT;
    DECLARE v_error_code TINYINT;
	DECLARE v_error_msg VARCHAR(200);
    
    SET v_count = 0;
    SET out_err_msg = '';
    SELECT 1 INTO v_count
        FROM t_account
        WHERE c_username = in_user_id and c_password = in_password limit 1;
    IF(v_count = 0) THEN
        SET out_result = -1;
        SET out_err_msg = 'Invalid Credentials';
    ELSE
        SET out_err_msg = 'Successfully Logged in';
		SET out_result = 1;
    END IF;        
    
END //
    
DELIMITER ;

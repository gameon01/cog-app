USE cog;
SELECT database();
DROP PROCEDURE IF EXISTS proc_cog_transfer;
DELIMITER //
CREATE PROCEDURE proc_cog_transfer( 
    IN      in_user_id VARCHAR(50) ,
    IN      in_account_number VARCHAR(50) ,
    IN      in_transfer_to VARCHAR(50) ,
	IN      in_transfer_from VARCHAR(50) ,	
	IN      in_amount double ,	
    OUT     out_result INT ,
    OUT     out_err_msg VARCHAR(500)
)
proc_cog_transfer:BEGIN
    DECLARE v_count TINYINT;
	DECLARE v_user_count TINYINT;
    DECLARE v_error_code TINYINT;
	DECLARE v_ben_name VARCHAR(200);
    DECLARE v_balance VARCHAR(200);
	DECLARE v_new_balance double;
	DECLARE v_ben_balance double;
	DECLARE v_ben_new_balance double;
	
	SET v_ben_balance = 0;
    SET v_count = 0; 
	SET v_user_count = 0;
    SET out_err_msg = '';
	
    SELECT 1 INTO v_count FROM t_beneficiary WHERE c_username = in_user_id and c_ben_acc_number = in_account_number;	
	SELECT c_balance INTO v_balance from t_balance where c_username = in_user_id ;
	SELECT 1 INTO v_user_count from t_account where c_accntno = in_account_number;
	IF(v_user_count = 0) THEN
		SET out_result = -1;
        SET out_err_msg = 'Target account not found';
	ELSEIF(v_balance < in_amount) THEN
		SET out_result = -2;
        SET out_err_msg = 'You do not have sufficient balance';
    ELSEIF(v_count = 0) THEN
        SET out_result = -3;
        SET out_err_msg = 'This beneficiary is not added, First add the beneficiary to transfer funds';
     ELSE
		SELECT c_username into v_ben_name from t_account where c_accntno = in_account_number;
		SELECT c_balance into v_ben_balance from t_balance where c_username = v_ben_name;
		if(v_ben_balance = 0) THEN
		INSERT INTO t_balance(c_username, c_balance, c_create_time) VALUES (v_ben_name, v_ben_balance, CURRENT_TIMESTAMP );
		end if;
		set v_new_balance = v_balance - in_amount;
		set v_ben_new_balance = v_ben_balance + in_amount;
		update t_balance set c_balance = v_new_balance where c_username = in_user_id;
		update t_balance set c_balance = v_ben_new_balance where c_username = v_ben_name;
        INSERT INTO t_transaction_history(c_username, c_old_balance, c_new_balance, c_txn_type, c_transfor_to, c_transfor_from, c_txn_date) VALUES(in_user_id, v_ben_balance, v_new_balance, 'transfer', v_ben_name, 'self', CURRENT_TIMESTAMP);

		INSERT INTO t_transaction_history(c_username, c_old_balance, c_new_balance, c_txn_type, c_transfor_to, c_transfor_from, c_txn_date) VALUES(v_ben_name, v_ben_balance, v_ben_new_balance, 'transfer', 'self', in_user_id, CURRENT_TIMESTAMP);

		SET out_result = 1;
    END IF;        
    
  COMMIT;
END //
    
DELIMITER ;

USE cog;
SELECT database();
DROP PROCEDURE IF EXISTS proc_cog_get_transaction_history;
DELIMITER //
CREATE PROCEDURE proc_cog_get_transaction_history( 
    IN      in_user_id VARCHAR(50) ,
	IN 		in_start_date TIMESTAMP,
	IN 		in_end_date TIMESTAMP,
	OUT     out_result INT ,
    OUT     out_err_msg VARCHAR(500)
)
proc_cog_get_transaction_history:BEGIN

    IF(in_start_date IS NULL AND in_end_date IS NULL) THEN
        SET in_start_date = current_timestamp()-INTERVAL 1 DAY;
        SET in_end_date = current_timestamp();        
    END IF;
    
	SELECT 
            c_old_balance,
			c_new_balance,
            in_user_id,
            c_txn_type,
            c_txn_id,
            c_transfor_to,
            c_transfor_from,
            c_txn_date
            FROM t_transaction_history
            WHERE c_username = in_user_id
            AND c_txn_date BETWEEN in_start_date AND in_end_date
            ORDER BY c_txn_date DESC;

    set out_result = 1;
	set out_err_msg = '';
	
END //
    
DELIMITER ;

USE cog;
SELECT database();
DROP PROCEDURE IF EXISTS proc_cog_delete_beneficiary;
DELIMITER //
CREATE PROCEDURE proc_cog_delete_beneficiary( 
    IN      in_user_id VARCHAR(50) ,
	IN      in_account_number VARCHAR(50) ,	
    OUT     out_result INT ,
    OUT     out_err_msg VARCHAR(500)
)
proc_cog_delete_beneficiary:BEGIN
    DECLARE v_count TINYINT;
    DECLARE v_error_code TINYINT;
	DECLARE v_error_msg VARCHAR(200);
    DECLARE v_id VARCHAR(200);
	
    SET v_count = 0;
    SET out_err_msg = '';
    SELECT 1 INTO v_count
        FROM t_beneficiary
        WHERE c_username = in_user_id and c_ben_acc_number = in_account_number;
    IF(v_count = 0) THEN
        SET out_result = -1;
        SET out_err_msg = 'beneficiary does not exist';
     ELSE
        SET v_count = 0;
		delete from t_beneficiary where c_username = in_user_id and c_ben_acc_number = in_account_number;
		SELECT LAST_INSERT_ID() INTO v_id;
		SET out_result = 1;
    END IF;        
    
  COMMIT;
END //
    
DELIMITER ;

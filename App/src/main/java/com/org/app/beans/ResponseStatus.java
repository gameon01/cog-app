package com.org.app.beans;

public class ResponseStatus {

    private String statusMessage;
    private int statusCode;

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public ResponseStatus(String statusMessage, int statusCode) {
        super();
        this.statusMessage = statusMessage;
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "ResponseStatus [statusMessage=" + statusMessage + ", statusCode=" + statusCode + "]";
    };

}
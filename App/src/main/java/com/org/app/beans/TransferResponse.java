package com.org.app.beans;

public class TransferResponse {
	private ResponseStatus status;
	private String transferMsg;

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public String getTransferMsg() {
		return transferMsg;
	}

	public void setTransferMsg(String transferMsg) {
		this.transferMsg = transferMsg;
	}
}

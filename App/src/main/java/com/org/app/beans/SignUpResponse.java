package com.org.app.beans;

public class SignUpResponse {
	private String status;
	private String errorCode;
	private String errorMsg;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	@Override
	public String toString() {
		return "SignUpResponse [status=" + status + ", errorCode=" + errorCode + ", errorMsg=" + errorMsg + "]";
	}

}

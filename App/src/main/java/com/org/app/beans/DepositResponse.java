package com.org.app.beans;

public class DepositResponse {
	private ResponseStatus status;
	private String depositStatus;

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public String getDepositStatus() {
		return depositStatus;
	}

	public void setDepositStatus(String depositStatus) {
		this.depositStatus = depositStatus;
	}

	@Override
	public String toString() {
		return "DepositResponse [status=" + status + ", depositStatus=" + depositStatus + "]";
	}
}

package com.org.app.beans;

import java.util.Date;

public class TransactionHistory {
	private double oldBalance;
	private double newBalance;
	private String transforTo;
	private String transforFrom;
	private int transactionId;
	private Date transactionDate;
	private String txnType;

	public TransactionHistory(){
		
	}
	public TransactionHistory(double oldBalance, double newBalance, String transforTo, String transforFrom,
			int transactionId, Date transactionDate) {
		super();
		this.oldBalance = oldBalance;
		this.newBalance = newBalance;
		this.transforTo = transforTo;
		this.transforFrom = transforFrom;
		this.transactionId = transactionId;
		this.transactionDate = transactionDate;
	}

	public double getOldBalance() {
		return oldBalance;
	}

	public void setOldBalance(double oldBalance) {
		this.oldBalance = oldBalance;
	}

	public double getNewBalance() {
		return newBalance;
	}

	public void setNewBalance(double newBalance) {
		this.newBalance = newBalance;
	}

	public String getTransforTo() {
		return transforTo;
	}

	public void setTransforTo(String transforTo) {
		this.transforTo = transforTo;
	}

	public String getTransforFrom() {
		return transforFrom;
	}

	public void setTransforFrom(String transforFrom) {
		this.transforFrom = transforFrom;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
}

package com.org.app.beans;

public class LoginResponse {
	private String AuthCode;
	private ResponseStatus responseStatus;

	public String getAuthCode() {
		return AuthCode;
	}

	public void setAuthCode(String authCode) {
		AuthCode = authCode;
	}

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	@Override
	public String toString() {
		return "AuthenticationResponse [AuthCode=" + AuthCode + ", responseStatus=" + responseStatus + "]";
	}

}

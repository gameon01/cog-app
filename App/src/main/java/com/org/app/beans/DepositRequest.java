package com.org.app.beans;

public class DepositRequest {
	private String authCode;
	private double amount;
	private String comments;

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "DepositRequest [authCode=" + authCode + ", amount=" + amount + ", comments=" + comments + "]";
	}

}

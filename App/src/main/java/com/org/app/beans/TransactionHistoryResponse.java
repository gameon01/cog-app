package com.org.app.beans;

import java.util.List;

public class TransactionHistoryResponse {
	private List<TransactionHistory> transactionHistory;
	private ResponseStatus status;

	public ResponseStatus getStatus() {
		return status;
	}

	public TransactionHistoryResponse() {
	}

	public TransactionHistoryResponse(List<TransactionHistory> transactionHistory) {
		super();
		this.transactionHistory = transactionHistory;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public List<TransactionHistory> getTransactionHistory() {
		return transactionHistory;
	}

	public void setTransactionHistory(List<TransactionHistory> transactionHistory) {
		this.transactionHistory = transactionHistory;
	}
}

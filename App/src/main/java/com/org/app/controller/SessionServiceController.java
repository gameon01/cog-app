package com.org.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.org.app.beans.LoginRequest;
import com.org.app.beans.LoginResponse;
import com.org.app.beans.LogoutRequest;
import com.org.app.beans.LogoutResponse;
import com.org.app.beans.SessionRequest;
import com.org.app.beans.SignUpRequest;
import com.org.app.beans.SignUpResponse;
import com.org.app.service.impl.SessionServiceImpl;


@Controller
public class SessionServiceController {
	@Autowired
	SessionServiceImpl service;

	@ResponseBody
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public SignUpResponse register(@RequestBody SignUpRequest signUpRequest) {
		System.out.println("allRequestParams = " + signUpRequest);
		return service.registerUser(signUpRequest);
	}

	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public LoginResponse login(@RequestBody LoginRequest loginRequest) {
		System.out.println("Login - RequestParams= " + loginRequest);
		return service.login(loginRequest);
	}

	@ResponseBody
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public LogoutResponse logoutAccount(@RequestBody LogoutRequest accountDetailsRequest) {
		System.out.println("Logout - RequestParams= " + accountDetailsRequest);
		return service.logoutUser(accountDetailsRequest);
	}

	@ResponseBody
	@RequestMapping(value = "/checkUserLoggedIn", method = RequestMethod.POST)
	public boolean checkUserLoggedIn(@RequestBody SessionRequest accountDetailsRequest) {
		System.out.println("allRequestParams = " + accountDetailsRequest);
		return service.checkUserLoggedIn(accountDetailsRequest);
	}
}
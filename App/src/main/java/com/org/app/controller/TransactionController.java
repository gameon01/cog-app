package com.org.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.org.app.beans.AddBeneficiaryRequest;
import com.org.app.beans.AddBeneficiaryResponse;
import com.org.app.beans.BalanceRequest;
import com.org.app.beans.BalanceResponse;
import com.org.app.beans.DeleteBeneficiaryRequest;
import com.org.app.beans.DeleteBeneficiaryResponse;
import com.org.app.beans.DepositRequest;
import com.org.app.beans.DepositResponse;
import com.org.app.beans.TransactionHistoryRequest;
import com.org.app.beans.TransactionHistoryResponse;
import com.org.app.beans.TransferRequest;
import com.org.app.beans.TransferResponse;
import com.org.app.service.impl.TransactionServiceImpl;

@Controller
public class TransactionController {
	@Autowired
	TransactionServiceImpl transactionService;

	@ResponseBody
	@RequestMapping(value = "/addBeneficiary", method = RequestMethod.POST)
	public AddBeneficiaryResponse addBeneficiary(@RequestBody AddBeneficiaryRequest addBeneficiaryRequest) {
		System.out.println("addBeneficiary : RequestParams = " + addBeneficiaryRequest);
		return transactionService.addBeneficiary(addBeneficiaryRequest);
	}

	@ResponseBody
	@RequestMapping(value = "/deleteBeneficiary", method = RequestMethod.POST)
	public DeleteBeneficiaryResponse deleteBeneficiary(@RequestBody DeleteBeneficiaryRequest deleteBeneficiaryRequest) {
		System.out.println("deleteBeneficiary : RequestParams = " + deleteBeneficiaryRequest);
		return transactionService.deleteBeneficiary(deleteBeneficiaryRequest);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deposit", method = RequestMethod.POST)
	public DepositResponse deposit(@RequestBody DepositRequest depositRequest) {
		System.out.println("deposit : RequestParams = " + depositRequest);
		return transactionService.deposit(depositRequest);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getBalance", method = RequestMethod.POST)
	public BalanceResponse getBalance(@RequestBody BalanceRequest balanceRequest) {
		System.out.println("getBalance : RequestParams = " + balanceRequest);
		return transactionService.getBalance(balanceRequest);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getTransactionHistory", method = RequestMethod.POST)
	public TransactionHistoryResponse getTransactionHistory(@RequestBody TransactionHistoryRequest transactionHistoryRequest) {
		System.out.println("getTransactionHistory : RequestParams = " + transactionHistoryRequest);
		return transactionService.getTransactionHistory(transactionHistoryRequest);
	}
	
	@ResponseBody
	@RequestMapping(value = "/transfer", method = RequestMethod.POST)
	public TransferResponse transfer(@RequestBody TransferRequest transferRequest) {
		System.out.println("getTransactionHistory : RequestParams = " + transferRequest);
		return transactionService.transfer(transferRequest);
	}
}

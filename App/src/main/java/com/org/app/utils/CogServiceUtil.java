package com.org.app.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.org.app.beans.SignUpRequest;

public class CogServiceUtil {
	static Map<String, String> userAuthCodeMapping = new HashMap<String, String>();

	public static String generateHashByEncoding(String input, String encoding) throws NoSuchAlgorithmException {
		MessageDigest ms = MessageDigest.getInstance(encoding);
		StringBuffer sb = new StringBuffer();
		byte[] mdbytes = ms.digest(input.getBytes());
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		System.out.println("generateHashByEncoding : " + sb.toString());
		return sb.toString();
	}

	public static boolean validateSignUpRequest(SignUpRequest signUpRequest) {
		if (signUpRequest != null && signUpRequest.getFirstName() != null
				&& signUpRequest.getFirstName().length() > 6) {
			if (signUpRequest.getPassword() != null && signUpRequest.getPassword().length() > 6
					&& signUpRequest.getAccountNo() != null && signUpRequest.getAccountNo().length() >= 1) {
				return true;
			}
		}
		return false;
	}

	public static String storeSession(String userName) {
		String authCode = null;
		authCode = "COG" + UUID.randomUUID().toString();
		userAuthCodeMapping.put(authCode, userName);
		return authCode;
	}

	public static String logout(String authCode) {
		return userAuthCodeMapping.remove(authCode);
	}
	
	public static boolean isValidSession(String authCode) {
		return userAuthCodeMapping.containsKey(authCode);
	}
	public static boolean validateUserSession(String authCode) {
		return userAuthCodeMapping.containsKey(authCode);
	}

	public static String getUser(String authCode) {
		return userAuthCodeMapping.get(authCode);
	}
}

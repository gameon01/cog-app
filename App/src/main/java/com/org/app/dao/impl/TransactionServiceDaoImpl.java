package com.org.app.dao.impl;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.org.app.beans.AddBeneficiaryRequest;
import com.org.app.beans.AddBeneficiaryResponse;
import com.org.app.beans.BalanceRequest;
import com.org.app.beans.BalanceResponse;
import com.org.app.beans.DeleteBeneficiaryRequest;
import com.org.app.beans.DeleteBeneficiaryResponse;
import com.org.app.beans.DepositRequest;
import com.org.app.beans.DepositResponse;
import com.org.app.beans.ResponseStatus;
import com.org.app.beans.TransactionHistory;
import com.org.app.beans.TransactionHistoryRequest;
import com.org.app.beans.TransactionHistoryResponse;
import com.org.app.beans.TransferRequest;
import com.org.app.beans.TransferResponse;
import com.org.app.dao.TransactionServiceDao;

public class TransactionServiceDaoImpl extends JdbcDaoSupport implements TransactionServiceDao {
	JdbcTemplate jdbcTemplate;

	public AddBeneficiaryResponse addBeneficiary(AddBeneficiaryRequest addBeneficiaryRequest, String userName) {
		AddBeneficiaryResponse response = new AddBeneficiaryResponse();
		String procName = "proc_cog_add_beneficiary";
		try {
			SimpleJdbcCall proc = new SimpleJdbcCall(getJdbcTemplate()).withProcedureName(procName);
			MapSqlParameterSource inParams = new MapSqlParameterSource();
			inParams.addValue("in_user_id", userName);
			inParams.addValue("in_beneficiary_name", addBeneficiaryRequest.getBeneficiaryName());
			inParams.addValue("in_account_number", addBeneficiaryRequest.getAccountNumber());
			inParams.addValue("in_ifsc_code", addBeneficiaryRequest.getIfscCode());
			inParams.addValue("in_nick_name", addBeneficiaryRequest.getNickName());
			Map<String, Object> outMap = proc.execute(inParams);
			int result = 1;
			String errorMsg = null;
			if (outMap != null) {
				result = (Integer) outMap.get("out_result");
				errorMsg = (String) outMap.get("out_err_msg");
			}
			if (result > 0) {
				ResponseStatus status = new ResponseStatus("Successfully Added", 200);
				response.setStatus(status);
			} else {
				ResponseStatus status = new ResponseStatus("Unable to add", 200);
				response.setStatus(status);
				response.setErrorMsg(errorMsg);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setErrorMsg("Technical Error");
		}
		return response;
	}

	public DepositResponse deposit(DepositRequest depositRequest, String userName) {
		DepositResponse response = new DepositResponse();
		String procName = "proc_cog_deposit";
		try {
			SimpleJdbcCall proc = new SimpleJdbcCall(getJdbcTemplate()).withProcedureName(procName);
			MapSqlParameterSource inParams = new MapSqlParameterSource();
			inParams.addValue("in_user_id", userName);
			inParams.addValue("in_amount", depositRequest.getAmount());
			inParams.addValue("in_txn_type", "self-deposit");
			Map<String, Object> outMap = proc.execute(inParams);
			int result = 1;
			String errorMsg = null;
			if (outMap != null) {
				result = (Integer) outMap.get("out_result");
				errorMsg = (String) outMap.get("out_err_msg");
			}
			if (result > 0) {
				ResponseStatus status = new ResponseStatus("Successfully Added", 200);
				response.setStatus(status);
			} else {
				ResponseStatus status = new ResponseStatus("Unable to add", 200);
				response.setStatus(status);
				response.setDepositStatus(errorMsg);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setDepositStatus("Technical Error");
		}
		return response;
	}

	public DeleteBeneficiaryResponse deleteBeneficiary(DeleteBeneficiaryRequest deleteBeneficiaryRequest,
			String userName) {

		DeleteBeneficiaryResponse response = new DeleteBeneficiaryResponse();
		String procName = "proc_cog_delete_beneficiary";
		try {
			SimpleJdbcCall proc = new SimpleJdbcCall(getJdbcTemplate()).withProcedureName(procName);
			MapSqlParameterSource inParams = new MapSqlParameterSource();
			inParams.addValue("in_user_id", userName);
			inParams.addValue("in_account_number", deleteBeneficiaryRequest.getBeneficiaryAccNo());
			Map<String, Object> outMap = proc.execute(inParams);
			int result = 1;
			String errorMsg = null;
			if (outMap != null) {
				result = (Integer) outMap.get("out_result");
				errorMsg = (String) outMap.get("out_err_msg");
			}
			if (result > 0) {
				ResponseStatus status = new ResponseStatus("Beneficiary Successfully Removed", 200);
				response.setStatus(status);
			} else {
				ResponseStatus status = new ResponseStatus("Unable to remove", 200);
				response.setStatus(status);
				response.setDeleteStatus(errorMsg);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setDeleteStatus("Technical Error");
		}
		return response;
	}

	public BalanceResponse getBalance(BalanceRequest balanceRequest, String userName) {
		BalanceResponse response = new BalanceResponse();
		String procName = "proc_cog_get_balance";
		try {
			SimpleJdbcCall proc = new SimpleJdbcCall(getJdbcTemplate()).withProcedureName(procName);
			MapSqlParameterSource inParams = new MapSqlParameterSource();
			inParams.addValue("in_user_id", userName);
			Map<String, Object> outMap = proc.execute(inParams);
			int result = 1;
			String errorMsg = null;
			double balance = 0;
			if (outMap != null) {
				result = (Integer) outMap.get("out_result");
				errorMsg = (String) outMap.get("out_err_msg");
				balance = (Double) outMap.get("out_balance");
			}
			if (result > 0) {
				ResponseStatus status = new ResponseStatus("Successfull", 200);
				response.setStatus(status);
				response.setBalance(balance);
			} else {
				ResponseStatus status = new ResponseStatus(errorMsg, 200);
				response.setStatus(status);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ResponseStatus status = new ResponseStatus("Technical Error", 200);
			response.setStatus(status);
		}
		return response;

	}

	public TransactionHistoryResponse getTransactionHistory(TransactionHistoryRequest transactionHistoryRequest,
			String userName) {
		TransactionHistoryResponse response = new TransactionHistoryResponse();
		List<TransactionHistory> recordLists = new ArrayList<TransactionHistory>();
		String procName = "proc_cog_get_transaction_history";
		try {
			SimpleJdbcCall proc = new SimpleJdbcCall(getJdbcTemplate()).withProcedureName(procName);
			proc.returningResultSet("history", new RowMapper<TransactionHistory>() {
				public TransactionHistory mapRow(java.sql.ResultSet resultSet, int rowNum) throws SQLException {
					TransactionHistory record = new TransactionHistory();
					record.setNewBalance(resultSet.getDouble("c_new_balance"));
					record.setOldBalance(resultSet.getDouble("c_old_balance"));
					record.setTransactionId(resultSet.getInt("c_txn_id"));
					record.setTransforFrom(resultSet.getString("c_transfor_from"));
					record.setTransforTo(resultSet.getString("c_transfor_to"));
					record.setTxnType(resultSet.getString("c_txn_type"));
					record.setTransactionDate(resultSet.getDate("c_txn_date"));
					return record;
				}
			});
			MapSqlParameterSource inParams = new MapSqlParameterSource();
			Date startDate = new Date(Long.parseLong(transactionHistoryRequest.getStartDate()));
			Date endDate = new Date(Long.parseLong(transactionHistoryRequest.getEndDate()));
			System.out.println("start date :"+startDate+" endDate : "+endDate);
			inParams.addValue("in_user_id", userName);
			inParams.addValue("in_start_date", startDate);
			inParams.addValue("in_end_date", endDate);
			
			Map<String, Object> outMap = proc.execute(inParams);

			int result = (Integer) outMap.get("out_result");
			System.out.println("result :"+result);
			if (result == 1) {
				@SuppressWarnings("unchecked")
				List<TransactionHistory> recordList = (List<TransactionHistory>) outMap.get("history");
				recordLists.addAll(recordList);
				response = new TransactionHistoryResponse(recordLists);
		}
		Collections.sort(recordLists, new Comparator<TransactionHistory>() {
			public int compare(TransactionHistory t1, TransactionHistory t2) {
				return (int) (t2.getTransactionId() - t1.getTransactionId());
			}
		});
			
			String errorMsg = null;
			if (outMap != null) {
				result = (Integer) outMap.get("out_result");
				errorMsg = (String) outMap.get("out_err_msg");
			}
			if (result > 0) {
				ResponseStatus status = new ResponseStatus("Successfull", 200);
				response.setStatus(status);
			} else {
				ResponseStatus status = new ResponseStatus(errorMsg, 200);
				response.setStatus(status);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ResponseStatus status = new ResponseStatus("Technical Error", 200);
			response.setStatus(status);
		}
		return response;
	}

	public TransferResponse transfer(TransferRequest transferRequest, String userName) {
		TransferResponse response = new TransferResponse();
		String procName = "proc_cog_transfer";
		try {
			SimpleJdbcCall proc = new SimpleJdbcCall(getJdbcTemplate()).withProcedureName(procName);
			MapSqlParameterSource inParams = new MapSqlParameterSource();
			inParams.addValue("in_user_id", userName);
			inParams.addValue("in_account_number", transferRequest.getBeneficiaryAccNo());
			inParams.addValue("in_amount", transferRequest.getAmount());
			inParams.addValue("in_transfer_to", transferRequest.getBeneficiaryAccNo());
			inParams.addValue("in_transfer_from", userName);
			
			Map<String, Object> outMap = proc.execute(inParams);
			int result = 1;
			String errorMsg = null;
			if (outMap != null) {
				result = (Integer) outMap.get("out_result");
				errorMsg = (String) outMap.get("out_err_msg");
			}
			if (result > 0) {
				ResponseStatus status = new ResponseStatus("Successfully Transfered", 200);
				response.setStatus(status);
			} else {
				ResponseStatus status = new ResponseStatus("Unable to transfer ", 200);
				response.setStatus(status);
				response.setTransferMsg(errorMsg);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setTransferMsg("Technical Error");
		}
		return response;
	}
}
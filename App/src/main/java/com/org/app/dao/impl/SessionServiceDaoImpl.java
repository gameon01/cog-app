package com.org.app.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.org.app.beans.LoginRequest;
import com.org.app.beans.LoginResponse;
import com.org.app.beans.LogoutRequest;
import com.org.app.beans.LogoutResponse;
import com.org.app.beans.ResponseStatus;
import com.org.app.beans.SessionRequest;
import com.org.app.beans.SignUpRequest;
import com.org.app.beans.SignUpResponse;
import com.org.app.constants.Codes;
import com.org.app.dao.SessionServiceDao;
import com.org.app.utils.CogServiceUtil;

public class SessionServiceDaoImpl extends JdbcDaoSupport implements SessionServiceDao {
	JdbcTemplate jdbcTemplate;
	private Map<String, String> userAuthCodeMapping = new HashMap<String, String>();

	public LoginResponse login(LoginRequest loginRequest) {
		LoginResponse loginResponse = new LoginResponse();
			String procName = "proc_cog_login_user";
			try {
				SimpleJdbcCall proc = new SimpleJdbcCall(getJdbcTemplate()).withProcedureName(procName);
				MapSqlParameterSource inParams = new MapSqlParameterSource();
				inParams.addValue("in_user_id", loginRequest.getUserName());
				inParams.addValue("in_password",
						CogServiceUtil.generateHashByEncoding(loginRequest.getPassword(), "MD5"));
				Map<String, Object> outMap = proc.execute(inParams);
				int result = 0;
				String errorMsg = null;
				if (outMap != null) {
					result = (Integer) outMap.get("out_result");
					errorMsg = (String) outMap.get("out_err_msg");
				}
				if (result > 0) {
					String authCode = CogServiceUtil.storeSession(loginRequest.getUserName()); 
					loginResponse.setAuthCode(authCode);
					ResponseStatus responseStatus = new ResponseStatus(errorMsg, 200);
					loginResponse.setResponseStatus(responseStatus);
					return loginResponse;
				} else {
					ResponseStatus responseStatus = new ResponseStatus(errorMsg, 200);
					loginResponse.setResponseStatus(responseStatus);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		return loginResponse;

	}

	public LogoutResponse logout(LogoutRequest logoutRequest) {
		LogoutResponse response = new LogoutResponse();
		try {
			String code = CogServiceUtil.logout(logoutRequest.getAuthCode());
			System.out.println("code :"+code);
			if(code != null){
				response.setStatus(Codes.SUCCESS);
				response.setErrorMsg("Successfully logged out");
			}else{
				response.setStatus(Codes.FAILURE);
				response.setErrorMsg("Invalid Session");
			}
			
		} catch (Exception e) {
			e.getMessage();
			response.setErrorMsg(Codes.FAILURE);
		}
		return response;

	}

	public boolean checkUserLoggedIn(SessionRequest authCode) {
		try {
			return CogServiceUtil.validateUserSession(authCode.getAuthCode());
		} catch (Exception e) {
			e.getMessage();
		}
		return false;

	}

	public Double getBalance(LoginRequest accountDetailsRequest) {

		String query = "SELECT balance FROM balance where username='" + accountDetailsRequest.getUserName()
				+ "' limit 1";
		return jdbcTemplate.query(query, new ResultSetExtractor<Double>() {
			public Double extractData(ResultSet rs) throws SQLException, DataAccessException {
				return rs.next() ? rs.getDouble("balance") : -1;
			}
		});
	}

	public boolean isAuthenticated(String username, String AuthCode) {
		String storedAuthCode = userAuthCodeMapping.get(username);
		if (storedAuthCode != null && storedAuthCode.equals(AuthCode)) {
			return true;
		}
		return false;
	}

	public SignUpResponse registerUser(SignUpRequest signUpRequest) {
		SignUpResponse response = new SignUpResponse();
		String procName = "proc_cog_register_user";
		System.out.println("getJdbcTemplate() :" + getJdbcTemplate().getDataSource());
		try {
			SimpleJdbcCall proc = new SimpleJdbcCall(getJdbcTemplate()).withProcedureName(procName);
			MapSqlParameterSource inParams = new MapSqlParameterSource();
			inParams.addValue("in_user_id", signUpRequest.getUserName());
			inParams.addValue("in_password", CogServiceUtil.generateHashByEncoding(signUpRequest.getPassword(), "MD5"));
			inParams.addValue("in_first_name", signUpRequest.getFirstName());
			inParams.addValue("in_last_name", signUpRequest.getLastName());
			inParams.addValue("in_address", signUpRequest.getAddress());
			inParams.addValue("in_account_no", signUpRequest.getAccountNo());

			Map<String, Object> outMap = proc.execute(inParams);
			int result = 1;
			String errorMsg = null;
			if (outMap != null) {
				result = (Integer) outMap.get("out_result");
				errorMsg = (String) outMap.get("out_err_msg");
			}
			if (result == 0) {
				response.setStatus(Codes.SUCCESS);
				response.setErrorMsg("Successfully Registred");
			} else {
				response.setStatus(Codes.FAILURE);
				response.setErrorMsg(errorMsg);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Codes.FAILURE);
			response.setErrorMsg("Technical error");
		}
		return response;
	}

}

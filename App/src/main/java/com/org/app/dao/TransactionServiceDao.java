package com.org.app.dao;

import com.org.app.beans.AddBeneficiaryRequest;
import com.org.app.beans.AddBeneficiaryResponse;
import com.org.app.beans.BalanceRequest;
import com.org.app.beans.BalanceResponse;
import com.org.app.beans.DeleteBeneficiaryRequest;
import com.org.app.beans.DeleteBeneficiaryResponse;
import com.org.app.beans.DepositRequest;
import com.org.app.beans.DepositResponse;
import com.org.app.beans.TransactionHistoryRequest;
import com.org.app.beans.TransactionHistoryResponse;
import com.org.app.beans.TransferRequest;
import com.org.app.beans.TransferResponse;

public interface TransactionServiceDao {

	AddBeneficiaryResponse addBeneficiary(AddBeneficiaryRequest addBeneficiaryRequest, String userName);

	DepositResponse deposit(DepositRequest depositRequest, String userName);

	DeleteBeneficiaryResponse deleteBeneficiary(DeleteBeneficiaryRequest deleteBeneficiaryRequest, String userName);

	BalanceResponse getBalance(BalanceRequest balanceRequest, String userName);

	TransactionHistoryResponse getTransactionHistory(TransactionHistoryRequest transactionHistoryRequest,
			String userName);

	TransferResponse transfer(TransferRequest transferRequest, String userName);

}

package com.org.app.dao;

import com.org.app.beans.LoginRequest;
import com.org.app.beans.LoginResponse;
import com.org.app.beans.LogoutRequest;
import com.org.app.beans.LogoutResponse;
import com.org.app.beans.SessionRequest;
import com.org.app.beans.SignUpRequest;
import com.org.app.beans.SignUpResponse;

public interface SessionServiceDao {
	public LoginResponse login(LoginRequest accountDetailsRequest);
	public LogoutResponse logout(LogoutRequest logoutRequest);
	public boolean checkUserLoggedIn(SessionRequest request);
	public Double getBalance(LoginRequest accountDetailsRequest);
	public SignUpResponse registerUser(SignUpRequest signUpRequest);
}

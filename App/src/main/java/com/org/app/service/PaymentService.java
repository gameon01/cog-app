package com.org.app.service;

import java.sql.SQLException;

public interface PaymentService {
	public boolean updateSupplierConfigs(String configs) throws SQLException;
}

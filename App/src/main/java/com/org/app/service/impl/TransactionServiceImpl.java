package com.org.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.org.app.beans.AddBeneficiaryRequest;
import com.org.app.beans.AddBeneficiaryResponse;
import com.org.app.beans.BalanceRequest;
import com.org.app.beans.BalanceResponse;
import com.org.app.beans.DeleteBeneficiaryRequest;
import com.org.app.beans.DeleteBeneficiaryResponse;
import com.org.app.beans.DepositRequest;
import com.org.app.beans.DepositResponse;
import com.org.app.beans.ResponseStatus;
import com.org.app.beans.TransactionHistoryRequest;
import com.org.app.beans.TransactionHistoryResponse;
import com.org.app.beans.TransferRequest;
import com.org.app.beans.TransferResponse;
import com.org.app.dao.TransactionServiceDao;
import com.org.app.utils.CogServiceUtil;

public class TransactionServiceImpl {
	@Autowired
	TransactionServiceDao transactionDao;
	
	public AddBeneficiaryResponse addBeneficiary(AddBeneficiaryRequest addBeneficiaryRequest) {
		String userName = CogServiceUtil.getUser(addBeneficiaryRequest.getAuthCode());
		AddBeneficiaryResponse response = new AddBeneficiaryResponse();
		if(userName != null){
			response = transactionDao.addBeneficiary(addBeneficiaryRequest, userName);
		}else{
			response.setErrorMsg("User not logged in");
		}
		return response;
	}

	public DeleteBeneficiaryResponse deleteBeneficiary(DeleteBeneficiaryRequest deleteBeneficiaryRequest) {
		String userName = CogServiceUtil.getUser(deleteBeneficiaryRequest.getAuthCode());
		DeleteBeneficiaryResponse response = new DeleteBeneficiaryResponse();
		if(userName != null){
			response = transactionDao.deleteBeneficiary(deleteBeneficiaryRequest, userName);
		}else{
			response.setDeleteStatus("User not logged in");
		}
		return response;
	}

	public DepositResponse deposit(DepositRequest depositRequest) {
		String userName = CogServiceUtil.getUser(depositRequest.getAuthCode());
		DepositResponse response = new DepositResponse();
		if(userName != null){
			response  = transactionDao.deposit(depositRequest, userName);
		}else{
			response.setDepositStatus("User not logged in");
		}
		return response;
	}

	public BalanceResponse getBalance(BalanceRequest balanceRequest) {
		String userName = CogServiceUtil.getUser(balanceRequest.getAuthCode());
		BalanceResponse response = new BalanceResponse();
		if(userName != null){
			response  = transactionDao.getBalance(balanceRequest, userName);
		}else{
			ResponseStatus status = new ResponseStatus("Please login to verify the balance", 200);
			response.setStatus(status);

		}
		return response;
	}

	public TransactionHistoryResponse getTransactionHistory(TransactionHistoryRequest transactionHistoryRequest) {
		String userName = CogServiceUtil.getUser(transactionHistoryRequest.getAuthCode());
		TransactionHistoryResponse response = new TransactionHistoryResponse();
		if(userName != null){
			response  = transactionDao.getTransactionHistory(transactionHistoryRequest, userName);
		}else{
			ResponseStatus status = new ResponseStatus("Please login to verify the balance", 200);
			response.setStatus(status);

		}
		return response;
	}

	public TransferResponse transfer(TransferRequest transferRequest) {
		String userName = CogServiceUtil.getUser(transferRequest.getAuthCode());
		TransferResponse response = new TransferResponse();
		if(userName != null){
			response  = transactionDao.transfer(transferRequest, userName);
		}else{
			ResponseStatus status = new ResponseStatus("Please login to verify the balance", 200);
			response.setStatus(status);

		}
		return response;
	}

}

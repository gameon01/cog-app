package com.org.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.org.app.beans.LoginRequest;
import com.org.app.beans.LoginResponse;
import com.org.app.beans.LogoutRequest;
import com.org.app.beans.LogoutResponse;
import com.org.app.beans.SessionRequest;
import com.org.app.beans.SignUpRequest;
import com.org.app.beans.SignUpResponse;
import com.org.app.constants.Codes;
import com.org.app.dao.SessionServiceDao;
import com.org.app.utils.CogServiceUtil;

public class SessionServiceImpl {

	@Autowired
	SessionServiceDao bankDao;

	public SessionServiceImpl() {
		
	}

	public LoginResponse login(LoginRequest accountDetailsRequest) {
		return bankDao.login(accountDetailsRequest);
	}

	public LogoutResponse logoutUser(LogoutRequest accountDetailsRequest) {
		return bankDao.logout(accountDetailsRequest);
	}

	public boolean checkUserLoggedIn(SessionRequest accountDetailsRequest) {
		return bankDao.checkUserLoggedIn(accountDetailsRequest);
	}

	public Double getBalance(LoginRequest accountDetailsRequest) {
		return bankDao.getBalance(accountDetailsRequest);
	}

	public SignUpResponse registerUser(SignUpRequest signUpRequest) {
		boolean isValid = CogServiceUtil.validateSignUpRequest(signUpRequest);
		SignUpResponse response = new SignUpResponse();
		if (isValid) {
			response = bankDao.registerUser(signUpRequest);
		} else {
			response.setStatus(Codes.FAILURE);
			response.setErrorMsg("invalid request");
		}
		return response;
	}

}

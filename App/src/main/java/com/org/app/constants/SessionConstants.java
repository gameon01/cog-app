package com.org.app.constants;

public class SessionConstants {

	public static final String DRAW_RESULT = "DRAW_RESULT";
	public static String AUTHCODE_SUFFIX = "_AUTHCODE";

	public static enum RedisKeys {
		USER_DATA,
		LOTTERY_DATA,
		LOTTERY_DRAW_DATA,
		PACKAGE_DATA,
		EXT_ID_DATA,
		REMINDER_TIME_BY_EXTERNAL_ID,
		SESSION_ID_BY_LAST_ACCESSED,
		TEMP_TOKEN_CREATE_TIME
	}

	public static final String CONTROLLER_TIMER_LOGGER = "CONTROLLER_TIMER_LOGGER";
}

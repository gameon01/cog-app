cognetive scale - Case Study
============================

Used technologies :

Java 8, Mysql 5.7, Spring, Spring REST, apache datasource, Spring tool suite server( build on top of tomcat).

My free aws server got expired, so not able to deploy it on aws cloude.

Run Instructions:
Install mysql server and create a schema with name "cog".
Run the db-data.sql file which is there in DB folder to create tables.
Import this (only App) project as maven project and run on tomcat.

Use postman to execute the API's
